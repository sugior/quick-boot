package com.quick.online.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.quick.online.entity.SysAccessSchema;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysAccessSchemaMapper extends BaseMapper<SysAccessSchema> {

}
